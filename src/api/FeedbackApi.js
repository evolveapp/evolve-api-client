/*
 * Evolve API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Feedback', 'model/FeedbackRequest'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../models/Feedback'), require('../models/FeedbackRequest'));
  } else {
    // Browser globals (root is window)
    if (!root.EvolveApi) {
      root.EvolveApi = {};
    }
    root.EvolveApi.FeedbackApi = factory(root.EvolveApi.ApiClient, root.EvolveApi.Feedback, root.EvolveApi.FeedbackRequest);
  }
}(this, function(ApiClient, Feedback, FeedbackRequest) {
  'use strict';

  /**
   * Feedback service.
   * @module api/FeedbackApi
   * @version v1.1
   */

  /**
   * Constructs a new FeedbackApi.
   * @alias module:api/FeedbackApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the feedbackList operation.
     * @callback module:api/FeedbackApi~feedbackListCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/Feedback>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {module:api/FeedbackApi~feedbackListCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/Feedback>}
     */
    this.feedbackList = function(callback) {
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [Feedback];

      return this.apiClient.callApi(
        '/feedback/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the feedbackRead operation.
     * @callback module:api/FeedbackApi~feedbackReadCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Feedback} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {String} id
     * @param {module:api/FeedbackApi~feedbackReadCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Feedback}
     */
    this.feedbackRead = function(id, callback) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling feedbackRead");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Feedback;

      return this.apiClient.callApi(
        '/feedback/{id}/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the feedbackRequestCreate operation.
     * @callback module:api/FeedbackApi~feedbackRequestCreateCallback
     * @param {String} error Error message, if any.
     * @param {module:model/FeedbackRequest} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {module:model/FeedbackRequest} data
     * @param {module:api/FeedbackApi~feedbackRequestCreateCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/FeedbackRequest}
     */
    this.feedbackRequestCreate = function(data, callback) {
      var postBody = data;

      // verify the required parameter 'data' is set
      if (data === undefined || data === null) {
        throw new Error("Missing the required parameter 'data' when calling feedbackRequestCreate");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = FeedbackRequest;

      return this.apiClient.callApi(
        '/feedback/request/', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the feedbackRequestList operation.
     * @callback module:api/FeedbackApi~feedbackRequestListCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/FeedbackRequest>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {module:api/FeedbackApi~feedbackRequestListCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/FeedbackRequest>}
     */
    this.feedbackRequestList = function(callback) {
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = [FeedbackRequest];

      return this.apiClient.callApi(
        '/feedback/request/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the feedbackRequestRead operation.
     * @callback module:api/FeedbackApi~feedbackRequestReadCallback
     * @param {String} error Error message, if any.
     * @param {module:model/FeedbackRequest} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {String} id
     * @param {module:api/FeedbackApi~feedbackRequestReadCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/FeedbackRequest}
     */
    this.feedbackRequestRead = function(id, callback) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling feedbackRequestRead");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = FeedbackRequest;

      return this.apiClient.callApi(
        '/feedback/request/{id}/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the feedbackRequestRespond operation.
     * @callback module:api/FeedbackApi~feedbackRequestRespondCallback
     * @param {String} error Error message, if any.
     * @param {module:model/FeedbackRequest} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     *
     * @param {String} id
     * @param {module:model/FeedbackRequest} data
     * @param {module:api/FeedbackApi~feedbackRequestRespondCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/FeedbackRequest}
     */
    this.feedbackRequestRespond = function(id, data, callback) {
      var postBody = data;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling feedbackRequestRespond");
      }

      // verify the required parameter 'data' is set
      if (data === undefined || data === null) {
        throw new Error("Missing the required parameter 'data' when calling feedbackRequestRespond");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['Bearer'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = FeedbackRequest;

      return this.apiClient.callApi(
        '/feedback/request/{id}/respond/', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
