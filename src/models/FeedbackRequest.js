/*
 * Evolve API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.1
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.13
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/User'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./User'));
  } else {
    // Browser globals (root is window)
    if (!root.EvolveApi) {
      root.EvolveApi = {};
    }
    root.EvolveApi.FeedbackRequest = factory(root.EvolveApi.ApiClient, root.EvolveApi.User);
  }
}(this, function(ApiClient, User) {
  'use strict';

  /**
   * The FeedbackRequest model module.
   * @module model/FeedbackRequest
   * @version v1.1
   */

  /**
   * Constructs a new <code>FeedbackRequest</code>.
   * @alias module:model/FeedbackRequest
   * @class
   * @param source {module:model/FeedbackRequest.SourceEnum} 
   * @param comment {String} 
   * @param recipientId {Number} 
   */
  var exports = function(source, comment, recipientId) {
    this.source = source;
    this.comment = comment;
    this.recipientId = recipientId;
  };

  /**
   * Constructs a <code>FeedbackRequest</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/FeedbackRequest} obj Optional instance to populate.
   * @return {module:model/FeedbackRequest} The populated <code>FeedbackRequest</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('source'))
        obj.source = ApiClient.convertToType(data['source'], 'String');
      if (data.hasOwnProperty('comment'))
        obj.comment = ApiClient.convertToType(data['comment'], 'String');
      if (data.hasOwnProperty('recipient_id'))
        obj.recipientId = ApiClient.convertToType(data['recipient_id'], 'Number');
      if (data.hasOwnProperty('requester_id'))
        obj.requesterId = ApiClient.convertToType(data['requester_id'], 'Number');
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'Number');
      if (data.hasOwnProperty('requester'))
        obj.requester = User.constructFromObject(data['requester']);
      if (data.hasOwnProperty('recipient'))
        obj.recipient = User.constructFromObject(data['recipient']);
      if (data.hasOwnProperty('created_at'))
        obj.createdAt = ApiClient.convertToType(data['created_at'], 'Date');
      if (data.hasOwnProperty('updated_at'))
        obj.updatedAt = ApiClient.convertToType(data['updated_at'], 'Date');
    }
    return obj;
  }

  /**
   * @member {module:model/FeedbackRequest.SourceEnum} source
   */
  exports.prototype.source = undefined;

  /**
   * @member {String} comment
   */
  exports.prototype.comment = undefined;

  /**
   * @member {Number} recipientId
   */
  exports.prototype.recipientId = undefined;

  /**
   * @member {Number} requesterId
   */
  exports.prototype.requesterId = undefined;

  /**
   * @member {Number} id
   */
  exports.prototype.id = undefined;

  /**
   * @member {module:model/User} requester
   */
  exports.prototype.requester = undefined;

  /**
   * @member {module:model/User} recipient
   */
  exports.prototype.recipient = undefined;

  /**
   * @member {Date} createdAt
   */
  exports.prototype.createdAt = undefined;

  /**
   * @member {Date} updatedAt
   */
  exports.prototype.updatedAt = undefined;


  /**
   * Allowed values for the <code>source</code> property.
   * @enum {String}
   * @readonly
   */
  exports.SourceEnum = {
    /**
     * value: "chat"
     * @const
     */
    chat: "chat",

    /**
     * value: "phone"
     * @const
     */
    phone: "phone",

    /**
     * value: "event"
     * @const
     */
    event: "event",

    /**
     * value: "interview"
     * @const
     */
    interview: "interview"
  };

  return exports;

}));
