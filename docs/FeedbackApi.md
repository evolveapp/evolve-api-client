# EvolveApi.FeedbackApi

All URIs are relative to *http://127.0.0.1:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**feedbackList**](FeedbackApi.md#feedbackList) | **GET** /feedback/ | 
[**feedbackRead**](FeedbackApi.md#feedbackRead) | **GET** /feedback/{id}/ | 
[**feedbackRequestCreate**](FeedbackApi.md#feedbackRequestCreate) | **POST** /feedback/request/ | 
[**feedbackRequestList**](FeedbackApi.md#feedbackRequestList) | **GET** /feedback/request/ | 
[**feedbackRequestRead**](FeedbackApi.md#feedbackRequestRead) | **GET** /feedback/request/{id}/ | 
[**feedbackRequestRespond**](FeedbackApi.md#feedbackRequestRespond) | **POST** /feedback/request/{id}/respond/ | 


<a name="feedbackList"></a>
# **feedbackList**
> [Feedback] feedbackList()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Feedback]**](Feedback.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="feedbackRead"></a>
# **feedbackRead**
> Feedback feedbackRead(id)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackRead(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Feedback**](Feedback.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="feedbackRequestCreate"></a>
# **feedbackRequestCreate**
> FeedbackRequest feedbackRequestCreate(data)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var data = new EvolveApi.FeedbackRequest(); // FeedbackRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackRequestCreate(data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**FeedbackRequest**](FeedbackRequest.md)|  | 

### Return type

[**FeedbackRequest**](FeedbackRequest.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="feedbackRequestList"></a>
# **feedbackRequestList**
> [FeedbackRequest] feedbackRequestList()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackRequestList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[FeedbackRequest]**](FeedbackRequest.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="feedbackRequestRead"></a>
# **feedbackRequestRead**
> FeedbackRequest feedbackRequestRead(id)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackRequestRead(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**FeedbackRequest**](FeedbackRequest.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="feedbackRequestRespond"></a>
# **feedbackRequestRespond**
> FeedbackRequest feedbackRequestRespond(id, data)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.FeedbackApi();

var id = "id_example"; // String | 

var data = new EvolveApi.FeedbackRequest(); // FeedbackRequest | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.feedbackRequestRespond(id, data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **data** | [**FeedbackRequest**](FeedbackRequest.md)|  | 

### Return type

[**FeedbackRequest**](FeedbackRequest.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

