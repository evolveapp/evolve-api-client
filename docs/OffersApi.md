# EvolveApi.OffersApi

All URIs are relative to *http://127.0.0.1:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**offersAccept**](OffersApi.md#offersAccept) | **GET** /offers/{id}/accept/ | 
[**offersList**](OffersApi.md#offersList) | **GET** /offers/ | 
[**offersRead**](OffersApi.md#offersRead) | **GET** /offers/{id}/ | 
[**offersReject**](OffersApi.md#offersReject) | **GET** /offers/{id}/reject/ | 
[**offersVacanciesList**](OffersApi.md#offersVacanciesList) | **GET** /offers/vacancies/ | 
[**offersVacanciesRead**](OffersApi.md#offersVacanciesRead) | **GET** /offers/vacancies/{id}/ | 


<a name="offersAccept"></a>
# **offersAccept**
> CandidateIndicativeOffer offersAccept(id)



This view only gathers the offers received by the present user

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersAccept(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**CandidateIndicativeOffer**](CandidateIndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersList"></a>
# **offersList**
> [CandidateIndicativeOffer] offersList()



This view only gathers the offers received by the present user

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[CandidateIndicativeOffer]**](CandidateIndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersRead"></a>
# **offersRead**
> CandidateIndicativeOffer offersRead(id)



This view only gathers the offers received by the present user

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersRead(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**CandidateIndicativeOffer**](CandidateIndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersReject"></a>
# **offersReject**
> CandidateIndicativeOffer offersReject(id)



This view only gathers the offers received by the present user

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersReject(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**CandidateIndicativeOffer**](CandidateIndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersVacanciesList"></a>
# **offersVacanciesList**
> [Vacancy] offersVacanciesList()



This view only gathers the vacancies associated to an event the user is registered to attend

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersVacanciesList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Vacancy]**](Vacancy.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="offersVacanciesRead"></a>
# **offersVacanciesRead**
> Vacancy offersVacanciesRead(id)



This view only gathers the vacancies associated to an event the user is registered to attend

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.OffersApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.offersVacanciesRead(id, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Vacancy**](Vacancy.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

