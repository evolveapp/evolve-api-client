# EvolveApi.FeedbackRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | **String** |  | 
**comment** | **String** |  | 
**recipientId** | **Number** |  | 
**requesterId** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 
**requester** | [**User**](User.md) |  | [optional] 
**recipient** | [**User**](User.md) |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 


<a name="SourceEnum"></a>
## Enum: SourceEnum


* `chat` (value: `"chat"`)

* `phone` (value: `"phone"`)

* `event` (value: `"event"`)

* `interview` (value: `"interview"`)




