# EvolveApi.Cities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**lat** | **String** |  | 
**lon** | **String** |  | 


