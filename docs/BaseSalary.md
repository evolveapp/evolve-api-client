# EvolveApi.BaseSalary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **Number** |  | 
**currency** | **String** |  | 


<a name="CurrencyEnum"></a>
## Enum: CurrencyEnum


* `GBP` (value: `"GBP"`)




