# EvolveApi.SkillsFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**feedbackType** | **String** |  | 
**name** | **String** |  | 
**level** | **String** |  | 
**originalLevel** | **String** |  | [optional] 
**isExpertFeedback** | **Boolean** |  | [optional] 


<a name="FeedbackTypeEnum"></a>
## Enum: FeedbackTypeEnum


* `validation` (value: `"validation"`)

* `invalidation` (value: `"invalidation"`)

* `overvaluation` (value: `"overvaluation"`)

* `undervaluation` (value: `"undervaluation"`)




<a name="LevelEnum"></a>
## Enum: LevelEnum


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `worldClass` (value: `"world_class"`)




