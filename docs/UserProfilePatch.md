# EvolveApi.UserProfilePatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profilePicture** | **String** |  | 
**fullName** | **String** |  | [optional] 
**headline** | **String** |  | [optional] 
**seniority** | **String** |  | [optional] 
**introduction** | **String** |  | [optional] 
**skills** | **{String: String}** |  | 
**personality** | **[String]** |  | 
**workStyle** | [**WorkStyle**](WorkStyle.md) |  | [optional] 
**rolesSought** | [**[Role]**](Role.md) |  | 
**employmentTypeSought** | **[String]** |  | 
**workingEnvironment** | **[String]** |  | 
**rightToWork** | **Boolean** |  | [optional] 
**citiesSought** | [**[Cities]**](Cities.md) |  | 
**interviewAvailability** | [**InterviewAvailability**](InterviewAvailability.md) |  | 


<a name="SeniorityEnum"></a>
## Enum: SeniorityEnum


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `lead` (value: `"lead"`)




<a name="{String: String}"></a>
## Enum: {String: String}


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `lead` (value: `"lead"`)




<a name="[PersonalityEnum]"></a>
## Enum: [PersonalityEnum]


* `enthusiasm` (value: `"enthusiasm"`)

* `flexibility` (value: `"flexibility"`)

* `disciplined` (value: `"disciplined"`)

* `honesty` (value: `"honesty"`)

* `reliability` (value: `"reliability"`)

* `curious` (value: `"curious"`)

* `friendly` (value: `"friendly"`)

* `leadership` (value: `"leadership"`)

* `energetic` (value: `"energetic"`)

* `confidence` (value: `"confidence"`)

* `adaptability` (value: `"adaptability"`)




<a name="[EmploymentTypeSoughtEnum]"></a>
## Enum: [EmploymentTypeSoughtEnum]


* `permanent` (value: `"permanent"`)

* `contract` (value: `"contract"`)




<a name="[WorkingEnvironmentEnum]"></a>
## Enum: [WorkingEnvironmentEnum]


* `remote` (value: `"remote"`)

* `partRemote` (value: `"part-remote"`)

* `inHouse` (value: `"in-house"`)




