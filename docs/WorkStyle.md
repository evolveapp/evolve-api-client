# EvolveApi.WorkStyle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collaborative** | **Number** |  | 
**innovative** | **Number** |  | 
**creator** | **Number** |  | 
**thinker** | **Number** |  | 
**leader** | **Number** |  | 
**strategic** | **Number** |  | 


