# EvolveApi.UserProfileTopLevel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth0UserId** | **String** |  | [optional] 
**fullName** | **String** |  | [optional] 
**email** | **String** |  | 
**profilePicture** | **String** |  | [optional] 
**headline** | **String** |  | [optional] 
**skills** | **{String: String}** |  | 
**personality** | **[String]** |  | 
**feedbacksReceived** | [**[Feedback]**](Feedback.md) |  | 
**skillsMatrix** | **String** |  | [optional] 
**personalityMatrix** | **String** |  | [optional] 
**workStyle** | **Object** |  | [optional] 
**workingEnvironment** | **[String]** |  | [optional] 
**employmentTypeSought** | **[String]** |  | [optional] 
**interviewAvailability** | **Object** |  | [optional] 
**citiesSought** | [**[Cities]**](Cities.md) |  | 
**introduction** | **String** |  | [optional] 
**rightToWork** | **Boolean** |  | [optional] 
**rolesSought** | **Object** |  | [optional] 
**seniority** | **String** |  | [optional] 


<a name="{String: String}"></a>
## Enum: {String: String}


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `lead` (value: `"lead"`)




<a name="[WorkingEnvironmentEnum]"></a>
## Enum: [WorkingEnvironmentEnum]


* `remote` (value: `"remote"`)

* `partRemote` (value: `"part-remote"`)

* `inHouse` (value: `"in-house"`)




<a name="[EmploymentTypeSoughtEnum]"></a>
## Enum: [EmploymentTypeSoughtEnum]


* `permanent` (value: `"permanent"`)

* `contract` (value: `"contract"`)




<a name="SeniorityEnum"></a>
## Enum: SeniorityEnum


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `lead` (value: `"lead"`)




