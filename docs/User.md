# EvolveApi.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**auth0UserId** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**profile** | **{String: String}** |  | [optional] 


