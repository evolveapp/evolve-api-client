# EvolveApi.Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **String** |  | 
**minimumSalary** | [**BaseSalary**](BaseSalary.md) |  | 


