# EvolveApi.InterviewAvailability

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mondays** | [**[Slot]**](Slot.md) |  | [optional] 
**tuesdays** | [**[Slot]**](Slot.md) |  | [optional] 
**wednesdays** | [**[Slot]**](Slot.md) |  | [optional] 
**thursdays** | [**[Slot]**](Slot.md) |  | [optional] 
**fridays** | [**[Slot]**](Slot.md) |  | [optional] 
**saturdays** | [**[Slot]**](Slot.md) |  | [optional] 
**sundays** | [**[Slot]**](Slot.md) |  | [optional] 


