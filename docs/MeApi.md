# EvolveApi.MeApi

All URIs are relative to *http://127.0.0.1:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**meAuth0PostRegisterCreate**](MeApi.md#meAuth0PostRegisterCreate) | **POST** /me/auth0_post_register | 
[**meCheckRandomNicknameUniquenessList**](MeApi.md#meCheckRandomNicknameUniquenessList) | **GET** /me/check_random_nickname_uniqueness | 
[**meGenerateRandomAvatarRead**](MeApi.md#meGenerateRandomAvatarRead) | **GET** /me/generate_random_avatar/{image_key} | 
[**meGenerateRandomNicknameList**](MeApi.md#meGenerateRandomNicknameList) | **GET** /me/generate_random_nickname | 
[**meList**](MeApi.md#meList) | **GET** /me/ | 
[**mePartialUpdate**](MeApi.md#mePartialUpdate) | **PATCH** /me/ | 
[**meProfilePictureCreate**](MeApi.md#meProfilePictureCreate) | **POST** /me/profile-picture | 
[**meProfileRead**](MeApi.md#meProfileRead) | **GET** /me/profile/{tribe_id}/{user_id}/ | 
[**meSettingsAnonymityList**](MeApi.md#meSettingsAnonymityList) | **GET** /me/settings_anonymity | 
[**meSettingsAnonymityPartialUpdate**](MeApi.md#meSettingsAnonymityPartialUpdate) | **PATCH** /me/settings_anonymity | 


<a name="meAuth0PostRegisterCreate"></a>
# **meAuth0PostRegisterCreate**
> meAuth0PostRegisterCreate()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meAuth0PostRegisterCreate(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meCheckRandomNicknameUniquenessList"></a>
# **meCheckRandomNicknameUniquenessList**
> meCheckRandomNicknameUniquenessList(opts)



Check nickname unique

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var opts = { 
  'nickname': "nickname_example" // String | user nickname
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meCheckRandomNicknameUniquenessList(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nickname** | **String**| user nickname | [optional] 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meGenerateRandomAvatarRead"></a>
# **meGenerateRandomAvatarRead**
> meGenerateRandomAvatarRead(imageKey)



Random avatar generator

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var imageKey = "imageKey_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meGenerateRandomAvatarRead(imageKey, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **imageKey** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meGenerateRandomNicknameList"></a>
# **meGenerateRandomNicknameList**
> meGenerateRandomNicknameList()



Random nickname generator

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meGenerateRandomNicknameList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meList"></a>
# **meList**
> UserProfileTopLevel meList()



Get UserProfile info

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.meList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserProfileTopLevel**](UserProfileTopLevel.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="mePartialUpdate"></a>
# **mePartialUpdate**
> UserProfileTopLevel mePartialUpdate(data)



Update UserProfile info

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var data = new EvolveApi.UserProfilePatch(); // UserProfilePatch | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.mePartialUpdate(data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UserProfilePatch**](UserProfilePatch.md)|  | 

### Return type

[**UserProfileTopLevel**](UserProfileTopLevel.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meProfilePictureCreate"></a>
# **meProfilePictureCreate**
> meProfilePictureCreate()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meProfilePictureCreate(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="meProfileRead"></a>
# **meProfileRead**
> meProfileRead(tribeId, userId)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var tribeId = "tribeId_example"; // String | 

var userId = "userId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meProfileRead(tribeId, userId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tribeId** | **String**|  | 
 **userId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meSettingsAnonymityList"></a>
# **meSettingsAnonymityList**
> meSettingsAnonymityList()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meSettingsAnonymityList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="meSettingsAnonymityPartialUpdate"></a>
# **meSettingsAnonymityPartialUpdate**
> meSettingsAnonymityPartialUpdate()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.MeApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.meSettingsAnonymityPartialUpdate(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

