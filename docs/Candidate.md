# EvolveApi.Candidate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**lastLogin** | **Date** |  | [optional] 
**isSuperuser** | **Boolean** | Designates that this user has all permissions without explicitly assigning them. | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**auth0UserId** | **String** |  | 
**userType** | **String** |  | [optional] 
**email** | **String** |  | 
**fullName** | **String** |  | [optional] 
**profilePicture** | **String** |  | [optional] 
**introduction** | **String** |  | [optional] 
**headline** | **String** |  | [optional] 
**seniority** | **String** |  | [optional] 
**skills** | **Object** |  | [optional] 
**personality** | **[String]** |  | [optional] 
**workStyle** | **Object** |  | [optional] 
**rolesSought** | **Object** |  | [optional] 
**employmentTypeSought** | **[String]** |  | [optional] 
**workingEnvironment** | **[String]** |  | [optional] 
**rightToWork** | **Boolean** |  | [optional] 
**interviewAvailability** | **Object** |  | [optional] 
**profile** | **Object** |  | [optional] 
**isStaff** | **Boolean** |  | [optional] 
**role** | **Number** |  | [optional] 
**groups** | **[Number]** | The groups this user belongs to. A user will get all permissions granted to each of their groups. | [optional] 
**userPermissions** | **[Number]** | Specific permissions for this user. | [optional] 
**citiesSought** | **[Number]** |  | [optional] 


<a name="UserTypeEnum"></a>
## Enum: UserTypeEnum


* `talent` (value: `"talent"`)

* `recruiter` (value: `"recruiter"`)




<a name="SeniorityEnum"></a>
## Enum: SeniorityEnum


* `junior` (value: `"junior"`)

* `medium` (value: `"medium"`)

* `senior` (value: `"senior"`)

* `lead` (value: `"lead"`)




<a name="[PersonalityEnum]"></a>
## Enum: [PersonalityEnum]


* `enthusiasm` (value: `"enthusiasm"`)

* `flexibility` (value: `"flexibility"`)

* `disciplined` (value: `"disciplined"`)

* `honesty` (value: `"honesty"`)

* `reliability` (value: `"reliability"`)

* `curious` (value: `"curious"`)

* `friendly` (value: `"friendly"`)

* `leadership` (value: `"leadership"`)

* `energetic` (value: `"energetic"`)

* `confidence` (value: `"confidence"`)

* `adaptability` (value: `"adaptability"`)




<a name="[EmploymentTypeSoughtEnum]"></a>
## Enum: [EmploymentTypeSoughtEnum]


* `permanent` (value: `"permanent"`)

* `contract` (value: `"contract"`)




<a name="[WorkingEnvironmentEnum]"></a>
## Enum: [WorkingEnvironmentEnum]


* `remote` (value: `"remote"`)

* `partRemote` (value: `"part-remote"`)

* `inHouse` (value: `"in-house"`)




