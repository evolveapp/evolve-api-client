# EvolveApi.CandidateIndicativeOffer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**vacancy** | [**Vacancy**](Vacancy.md) |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**salary** | **String** |  | 
**benefits** | **String** |  | 
**terms** | **String** |  | 
**status** | **String** |  | [optional] 
**event** | **Number** |  | [optional] 
**candidate** | **Number** |  | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `pending` (value: `"pending"`)

* `accepted` (value: `"accepted"`)

* `rejected` (value: `"rejected"`)




