# EvolveApi.TribesApi

All URIs are relative to *http://127.0.0.1:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tribesDiscoveryList**](TribesApi.md#tribesDiscoveryList) | **GET** /tribes/discovery/ | 
[**tribesPreviewRead**](TribesApi.md#tribesPreviewRead) | **GET** /tribes/preview/{tribe_id}/ | 


<a name="tribesDiscoveryList"></a>
# **tribesDiscoveryList**
> tribesDiscoveryList()





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.TribesApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.tribesDiscoveryList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="tribesPreviewRead"></a>
# **tribesPreviewRead**
> tribesPreviewRead(tribeId)





### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.TribesApi();

var tribeId = "tribeId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.tribesPreviewRead(tribeId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tribeId** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

