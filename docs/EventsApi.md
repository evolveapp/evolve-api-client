# EvolveApi.EventsApi

All URIs are relative to *http://127.0.0.1:8000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventsAttendeesList**](EventsApi.md#eventsAttendeesList) | **GET** /events/{parent_lookup_event}/attendees/ | 
[**eventsAttendeesOffersCreate**](EventsApi.md#eventsAttendeesOffersCreate) | **POST** /events/{parent_lookup_event}/attendees/{parent_lookup_user}/offers/ | Create an offer
[**eventsAttendeesOffersList**](EventsApi.md#eventsAttendeesOffersList) | **GET** /events/{parent_lookup_event}/attendees/{parent_lookup_user}/offers/ | 
[**eventsAttendeesOffersRead**](EventsApi.md#eventsAttendeesOffersRead) | **GET** /events/{parent_lookup_event}/attendees/{parent_lookup_user}/offers/{id}/ | 
[**eventsAttendeesOffersStatus**](EventsApi.md#eventsAttendeesOffersStatus) | **GET** /events/{parent_lookup_event}/attendees/{parent_lookup_user}/offers/status/ | 
[**eventsAttendeesRead**](EventsApi.md#eventsAttendeesRead) | **GET** /events/{parent_lookup_event}/attendees/{id}/ | 
[**eventsCreate**](EventsApi.md#eventsCreate) | **POST** /events/ | 
[**eventsList**](EventsApi.md#eventsList) | **GET** /events/ | 
[**eventsPartialUpdate**](EventsApi.md#eventsPartialUpdate) | **PATCH** /events/{id}/ | 
[**eventsRead**](EventsApi.md#eventsRead) | **GET** /events/{id}/ | 
[**eventsVacanciesList**](EventsApi.md#eventsVacanciesList) | **GET** /events/{parent_lookup_event}/vacancies/ | 
[**eventsVacanciesRead**](EventsApi.md#eventsVacanciesRead) | **GET** /events/{parent_lookup_event}/vacancies/{id}/ | 


<a name="eventsAttendeesList"></a>
# **eventsAttendeesList**
> [Candidate] eventsAttendeesList(parentLookupEvent)



A view to show a list of partecipants to an event

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var parentLookupEvent = "parentLookupEvent_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesList(parentLookupEvent, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentLookupEvent** | **String**|  | 

### Return type

[**[Candidate]**](Candidate.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsAttendeesOffersCreate"></a>
# **eventsAttendeesOffersCreate**
> IndicativeOffersCreate eventsAttendeesOffersCreate(parentLookupEvent, parentLookupUser, data)

Create an offer

An offer cannot be made if: -   There's already an offer with an higher salary     for a candidate taking part to this event (regardless of vacancy) -   The recruiter doesn't own any of the vacancies in the present event -   The recruiter doesn't own the vacancy for which he's doing the offer -   TODO: Add conditions on time and expiration

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var parentLookupEvent = "parentLookupEvent_example"; // String | 

var parentLookupUser = "parentLookupUser_example"; // String | 

var data = new EvolveApi.IndicativeOffersCreate(); // IndicativeOffersCreate | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesOffersCreate(parentLookupEvent, parentLookupUser, data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentLookupEvent** | **String**|  | 
 **parentLookupUser** | **String**|  | 
 **data** | [**IndicativeOffersCreate**](IndicativeOffersCreate.md)|  | 

### Return type

[**IndicativeOffersCreate**](IndicativeOffersCreate.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsAttendeesOffersList"></a>
# **eventsAttendeesOffersList**
> [IndicativeOffer] eventsAttendeesOffersList(parentLookupEvent, parentLookupUser, )



A view to show a list of offers made from the current recruiter to the user (other employers' offer won't be visible)

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var parentLookupEvent = "parentLookupEvent_example"; // String | 

var parentLookupUser = "parentLookupUser_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesOffersList(parentLookupEvent, parentLookupUser, , callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentLookupEvent** | **String**|  | 
 **parentLookupUser** | **String**|  | 

### Return type

[**[IndicativeOffer]**](IndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsAttendeesOffersRead"></a>
# **eventsAttendeesOffersRead**
> IndicativeOffer eventsAttendeesOffersRead(id, parentLookupEvent, parentLookupUser)



A view to show a list of offers made from the current recruiter to the user (other employers' offer won't be visible)

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var id = "id_example"; // String | 

var parentLookupEvent = "parentLookupEvent_example"; // String | 

var parentLookupUser = "parentLookupUser_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesOffersRead(id, parentLookupEvent, parentLookupUser, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **parentLookupEvent** | **String**|  | 
 **parentLookupUser** | **String**|  | 

### Return type

[**IndicativeOffer**](IndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsAttendeesOffersStatus"></a>
# **eventsAttendeesOffersStatus**
> [IndicativeOffer] eventsAttendeesOffersStatus(parentLookupEvent, parentLookupUser)



Main endpoint to enquire about the status for a talent

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var parentLookupEvent = "parentLookupEvent_example"; // String | 

var parentLookupUser = "parentLookupUser_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesOffersStatus(parentLookupEvent, parentLookupUser, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentLookupEvent** | **String**|  | 
 **parentLookupUser** | **String**|  | 

### Return type

[**[IndicativeOffer]**](IndicativeOffer.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsAttendeesRead"></a>
# **eventsAttendeesRead**
> Candidate eventsAttendeesRead(id, parentLookupEvent)



A view to show a list of partecipants to an event

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var id = "id_example"; // String | 

var parentLookupEvent = "parentLookupEvent_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsAttendeesRead(id, parentLookupEvent, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **parentLookupEvent** | **String**|  | 

### Return type

[**Candidate**](Candidate.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsCreate"></a>
# **eventsCreate**
> Event eventsCreate(data)



A view to show a list of events currently available to a recruiter or a talent

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var data = new EvolveApi.Event(); // Event | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsCreate(data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Event**](Event.md)|  | 

### Return type

[**Event**](Event.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsList"></a>
# **eventsList**
> [Event] eventsList()



A view to show a list of events currently available to a recruiter or a talent

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsList(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Event]**](Event.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsPartialUpdate"></a>
# **eventsPartialUpdate**
> Event eventsPartialUpdate(id, data)



A view to show a list of events currently available to a recruiter or a talent

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var id = "id_example"; // String | 

var data = new EvolveApi.Event(); // Event | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsPartialUpdate(id, data, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **data** | [**Event**](Event.md)|  | 

### Return type

[**Event**](Event.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsRead"></a>
# **eventsRead**
> Event eventsRead(id, )



A view to show a list of events currently available to a recruiter or a talent

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var id = "id_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsRead(id, , callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

[**Event**](Event.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsVacanciesList"></a>
# **eventsVacanciesList**
> [Vacancy] eventsVacanciesList(parentLookupEvent)



A view to show a list of vacancies asscciates to an event (can be used to show a list of postiions under an event)

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var parentLookupEvent = "parentLookupEvent_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsVacanciesList(parentLookupEvent, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parentLookupEvent** | **String**|  | 

### Return type

[**[Vacancy]**](Vacancy.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="eventsVacanciesRead"></a>
# **eventsVacanciesRead**
> Vacancy eventsVacanciesRead(id, parentLookupEvent)



A view to show a list of vacancies asscciates to an event (can be used to show a list of postiions under an event)

### Example
```javascript
var EvolveApi = require('evolve_api');
var defaultClient = EvolveApi.ApiClient.instance;

// Configure API key authorization: Bearer
var Bearer = defaultClient.authentications['Bearer'];
Bearer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//Bearer.apiKeyPrefix = 'Token';

var apiInstance = new EvolveApi.EventsApi();

var id = "id_example"; // String | 

var parentLookupEvent = "parentLookupEvent_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.eventsVacanciesRead(id, parentLookupEvent, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **parentLookupEvent** | **String**|  | 

### Return type

[**Vacancy**](Vacancy.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

