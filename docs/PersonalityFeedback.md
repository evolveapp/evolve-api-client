# EvolveApi.PersonalityFeedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trait** | **String** |  | 
**present** | **Boolean** |  | 


