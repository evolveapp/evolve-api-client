# EvolveApi.IndicativeOffer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**salary** | **String** |  | 
**benefits** | **String** |  | 
**terms** | **String** |  | 
**status** | **String** |  | [optional] 
**event** | **Number** |  | [optional] 
**vacancy** | **Number** |  | 
**candidate** | **Number** |  | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `pending` (value: `"pending"`)

* `accepted` (value: `"accepted"`)

* `rejected` (value: `"rejected"`)




