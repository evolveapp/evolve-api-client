# EvolveApi.Vacancy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**title** | **String** |  | 
**location** | **String** |  | 
**salaryFrom** | **String** |  | 
**salaryTo** | **String** |  | 
**mandatorySkills** | **[String]** |  | [optional] 
**bonusSkills** | **[String]** |  | [optional] 
**description** | **String** |  | [optional] 
**terms** | **String** |  | [optional] 
**benefits** | **String** |  | [optional] 
**published** | **Boolean** |  | [optional] 
**recruiter** | **Number** |  | 


