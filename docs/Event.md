# EvolveApi.Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**title** | **String** |  | 
**eventStart** | **Date** |  | 
**eventEnd** | **Date** |  | 
**eventType** | **String** |  | [optional] 
**vacancy** | **[Number]** |  | 
**attendees** | **[Number]** |  | 


<a name="EventTypeEnum"></a>
## Enum: EventTypeEnum


* `_private` (value: `"private"`)

* `group` (value: `"group"`)




