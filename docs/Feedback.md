# EvolveApi.Feedback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**userId** | **Number** |  | [optional] 
**reviewerId** | **Number** |  | [optional] 
**requestId** | **Number** |  | 
**publicComment** | **String** |  | [optional] 
**privateComment** | **String** |  | [optional] 
**skills** | [**[SkillsFeedback]**](SkillsFeedback.md) |  | 
**personality** | [**[PersonalityFeedback]**](PersonalityFeedback.md) |  | 
**marketValue** | **Number** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 


